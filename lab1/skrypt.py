import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import random
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from scipy.spatial import distance
import matplotlib.animation as animation


delta = 0.05
x_size = 10
y_size = 10
obst_vect = [(random.randrange(-10,10), random.randrange(-10,10)), (random.randrange(-10,10), random.randrange(-10,10)), (random.randrange(-10,10),random.randrange(-10,10)), (random.randrange(-10,10),random.randrange(-10,10))]
start_point=(-10,random.randrange(-10,10))
finish_point=(10,random.randrange(-10,10))

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)
#Up=1/2*(np.subtract(start_point,finish_point))**2
koi=10
kp=0.03
do=1

def Dl(x1,x2):
	return distance.euclidean(x1,x2)

def Fp(x1,x2):
	return kp*Dl(x1,x2)

def Foi(x1,x2):
	if Dl(x1,x2) <= do :
		return koi*(1/Dl(x1,x2)-1/do)*(1/Dl(x1,x2)**2)
	elif Dl(x1,x2) > do : 
		return 0

def Tablica_Foi(x1,wect_przeszk):
	tablica_sil = 0
	for i in wect_przeszk:
		tablica_sil += Foi(x1,i)
	return tablica_sil
##############################################################33
def Up(x1,x2):
	u=(Dl(x1,x2))**2
	return 0.5*kp*u

def Dl_vec(v):
    return np.sqrt(v[0]**2 + v[1]**2)

def Dl_vec_znorm(v):
    dl = Dl_vec(v)
    v = (v[0]/dl, v[1]/dl)
    return v

def Odejm_vec(x1, x2):
    vec = (x2[0]-x1[0], x2[1]-x1[1])
    return vec

def Mnoz_vec(vec, f):
    v = (vec[0]*f, vec[1]*f)
    return v

def Sum_vec(v1, v2):
    vec = (v1[0]+v2[0], v1[1]+v2[1])
    return vec

def Mapa_potenc(x1, x2, vec):
    return Tablica_Foi(x1, vec) + Fp(x1, x2)

for i, iv in enumerate(X):
	for j, jv in enumerate(Y):
		py = jv[j]
		px = iv[i]
		Z[j][i] = Mapa_potenc((px, py), finish_point, obst_vect)

path = []
Pocz = start_point

i=1000
while i > 0:
    i = i-1
    if Pocz == finish_point:
        break

    Koniec=finish_point
    vp_f = Fp(Pocz,Koniec)
    vp = Odejm_vec(Pocz,Koniec)
    vp = Dl_vec_znorm(vp)
    vp = Mnoz_vec(vp, vp_f)
    vo_s = (0,0)

    for vec in obst_vect:

        vo_f = Foi(Pocz, vec)
        vo = Odejm_vec(Pocz, vec)

        if Dl_vec(vo) > do:
            vo_f = 0
        
        vo = Dl_vec_znorm(vo)
        vo = Mnoz_vec(vo, vo_f)
        vo_s = Sum_vec(vo_s, vo)
    
    v_dir = Dl_vec_znorm(Sum_vec(vp, Mnoz_vec(vo_s, -1)))

    ex = round(v_dir[0])
    ey = round(v_dir[1])
    Pocz = (Pocz[0]+ex/10, Pocz[1]+ey/10)

    Poczx = Pocz[0]
    Poczy = Pocz[1]
    Poczx = min(10, Poczx)
    Poczx = max(-10, Poczx)
    Poczy = min(10, Poczy)
    Poczy = max(-10, Poczy)
    Pocz = (Poczx, Poczy)
    path.append(Pocz)

for i in x:
	for j in y:
		pkt_x = int(i/delta+1/delta*10)
		pkt_y = int(j/delta+1/delta*10)
		Z[pkt_y,pkt_x] = Tablica_Foi((i,j),obst_vect)+Fp((i,j),finish_point)

# initialization function 

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
line, = ax.plot([], [], lw=2) 

ax.set_title('Metoda potencjałów')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=1, vmin=-1)

plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='red')

for obstacle in obst_vect:
    pass	#plt.plot(obstacle[0], obstacle[1], "or", color='black')

#for p in path:
#
 #   plt.plot(p[0], p[1], "or", color='black', markersize=2)

plt.colorbar(orientation='vertical')

def init(): 
    line.set_data([], []) 
    return line, 

ListaX, ListaY = [], [] 

def Animacja(i): 
    try:
        p = path[i] 
        x = p[0] 
        y = p[1] 
        ListaX.append(x) 
        ListaY.append(y) 
        line.set_data(ListaX, ListaY) 
        return line,
    except IndexError:
        return line,

anim = animation.FuncAnimation(fig, Animacja, init_func=init, frames=10000, interval=20, blit=True) 
plt.grid(True)
plt.show()
